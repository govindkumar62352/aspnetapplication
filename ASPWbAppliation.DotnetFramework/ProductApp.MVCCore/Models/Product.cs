﻿namespace ProductApp.MVCCore.Models
{
    public class Product
    {
        #region Properties
        public int Id { get; set; }
        public string? Name { get; set; }
        public string? Description { get; set; }
        public bool isAvailable { get; set; } = true;
        #endregion

    }
}
