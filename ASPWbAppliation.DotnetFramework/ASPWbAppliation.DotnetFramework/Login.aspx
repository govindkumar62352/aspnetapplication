﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="ASPWbAppliation.DotnetFramework.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .newStyle1 {
            font-style: italic;
            text-decoration: blink;
            color: #0000FF;
            margin-left: 50px;
        }

        .auto-style1 {
            width: 31%;
            height: 204px;
        }
        .auto-style2 {
            height: 232px;
            width: 401px;
        }
        .auto-style4 {
            width: 209px;
        }

        .auto-style5 {
            width: 26px;
        }

    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div style="margin-bottom: 438px" class="auto-style2">
            <h2><span class="newStyle5">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Login Form</span><table class="auto-style1">
                <tr>
                    <td class="auto-style5">
            <asp:Label ID="lblUserName" runat="server" Text="UserName"></asp:Label>
                    </td>
                    <td class="auto-style4">
            <asp:TextBox ID="txtUserName" runat="server" style="margin-left: 30px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style5">
            <asp:Label ID="lblPassword" runat="server" Text="Password"></asp:Label>
                    </td>
                    <td class="auto-style4">
            <asp:TextBox ID="txtPassword" runat="server" style="margin-left: 24px" OnTextChanged="txtPassword_TextChanged" TextMode="Password"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style5">
            <asp:Button ID="btnSubmit" runat="server" OnClick="btnSubmit_Click" Text="Submit" Width="123px" />
                    </td>
                    <td class="auto-style4">
            <asp:Button ID="btnCancel" runat="server" OnClick="btnCancel_Click" style="margin-left: 0px" Text="Cancel" Width="125px" />
                    </td>

                </tr>
                </table>
            </h2>
            <br />
            <br />
            <br />
            &nbsp;
            <br />
            <br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            </div>
    </form>
</body>
</html>
